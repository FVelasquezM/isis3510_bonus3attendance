package com.lab.bonus3attendance

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore

class MainActivity : AppCompatActivity() {


    private val USR_MAC = "DC-74-82-92-78-A2"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun attend(v: View){

        val db = FirebaseFirestore.getInstance()

        db.collection("attendanceList")
            .whereEqualTo("mac", USR_MAC)
            .get()
            .addOnSuccessListener { result ->
                val doc = result.first()
                val docData = doc.data

                docData["attended"] = docData["attended"] as Long + 1L
                db.collection("attendanceList").document(doc.id).update(docData)
                    .addOnSuccessListener {
                        Toast.makeText(this@MainActivity,  "Attended in the database", Toast.LENGTH_LONG).show()
                    }
                    .addOnFailureListener { e ->
                        Toast.makeText(this@MainActivity,  "Failed: " + e.message, Toast.LENGTH_LONG).show()
                    }
            }
            .addOnFailureListener{ e ->
                Toast.makeText(this@MainActivity,  "Failed: " + e.message, Toast.LENGTH_LONG).show()
            }


    }
}
